const Sequelize = require("sequelize");

const sequelize = new Sequelize("sequelize_training", "root","", {
    dialect: "mysql",
    host: "localhost",
    define: {
        timestaps: true,
    }
});

module.exports = sequelize;
