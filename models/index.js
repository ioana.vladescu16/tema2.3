const Sequelize = require("sequelize");
const db = require("../config/db");
const CrocodilModel = require("./crocodil");
const InsulaModel = require("./insula");

const Crocodil = CrocodilModel(db, Sequelize);
const Insula = InsulaModel(db, Sequelize);

Insula.hasMany(Crocodil, {
    foreignKey: "insulaId",
    as: "Crocodil",
});
Crocodil.belongsTo(Insula);

module.exports = {
    Insula,
    Crocodil,
    connection: db,
};