const CrocodilDb = require("../models").Crocodil;
const InsulaDb = require("../models").Insula;

const controller = {
    getAllCrocodiles: async (req, res) => {
        CrocodilDb.findAll()
        .then((crocodili) => {
            res.status(200).send(crocodili);
        })
        .catch((err) => {
            console.log(err);
            res.status(500).send({ message: "Server error!" });
        });
    },

    addCrocodile: async (req, res) => {
       const { idInsula, nume, prenume, varsta } = req.body;
       InsulaDb.findByPk(idInsula)
       .then((insula) => {
           console.log(insula);
           if(insula) {
               insula
               .createCrocodil({nume, prenume, varsta})
               .then((crocodil) => {
                   res.status(200).send(crocodil)
               })
               .catch((err) => {
                   console.log(err)
                   res.status(500).send({ message: "Server error!"});
               });
           } else {
               res.status(404).send({ message: "Insula not found!"});
           }
       })
       .catch((err) => {
           console.log(err);
           res.status(500).send({ message: "Server error!" });
       });
   },

   deleteCrocodiles: async (req, res) => {
       const { id } = req.params;
       if(!id) {
        res.status(400).send({ message: "ID not provided"});
       }
       CrocodilDb.destroy({ where: { id: id}})
       .then(() => {
           res.status(200).send({ message: "Crocodile deleted!"});
       })
       .catch((err) => {
           console.log(err);
           res.status(500).send({ message: "Server error!" });
       });
   },

   updateCrocodile: async (req, res) => {
       const { id } = req.params;
       const { nume } = req.body;
       if(!id) {
        res.status(400).send({ message: "ID not provided"});
       }
      CrocodilDb.findByPk(id).then((crocodil) => {
         crocodil.update({ nume });
         res.status(200).send(crocodil);
      })
    .catch((error) => {
       console.log(error);
       res.status(500).send({ message: "Server error!"});
    });

   },
};

module.exports = controller;
