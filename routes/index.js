const express = require('express');
const router = express.Router();
const insulaRouter = require("./insula");
const crocodilRouter = require("./crocodil");

router.use("/insule", insulaRouter);
router.use("/crocodili", crocodilRouter);

module.exports = router;
