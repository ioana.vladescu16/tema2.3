const express = require("express");
const router = express.Router();
const insulaController = require("../controllers").insula; 

router.get("/:id/crocodili", insulaController.getCrocosFromIsland);
router.get("/", insulaController.getAllIslands);
router.get("/:id", insulaController.getIslandById);
router.post("/", insulaController.addIsland);

module.exports = router;